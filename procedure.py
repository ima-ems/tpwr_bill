from db import Db
from newcrawlbill import CrawlBills
from datetime import date,timedelta
import time
#get the places information
def doPlaceInf(link,partInf,acc,pw,cr,db):
    try:
        dPage = cr.enterPage(link)
        data = cr.getDetailInf(dPage)
        places = cr.handlePlacesInf(data,partInf,acc,pw)
        db.insertPlaces(places)
    except Exception as e:
        with open('report.txt','a') as f:
            f.write( 'account:  '+acc + partInf['id']+'\nsomething wrong dealing with places\n'+str(e)+'#####\n')
            f.close()
            print('some thing wrong dealing with places')
def updatePlaceInf(link,partInf,acc,pw,cr,db):
    print('updating place information')
    try:
        dPage = cr.enterPage(link)
        data = cr.getDetailInf(dPage)
        places = cr.handlePlacesInf(data,partInf,acc,pw)
        db.updatePlace(places)
    except Exception as e:
        with open('report.txt','a') as f:
            now = time.time()    
            f.write(time.ctime(now) + '\naccount:  '+acc +'\n'+ partInf['id']+'\nsomething wrong updateingling with places\n'+str(e)+'#####\n')


#load the places_id in databases if it had been stored than ignore it, load all ids at once to make it more effcient
def doBillsInf(detailLinks,partInfs,acc,pw,cr,db):
    print('downloadinf bills')
    dup = 0
    ids = db.getBillIds(partInfs[0]['id'])
    if len(ids) == len(partInfs):
        return
    for i,dLink in enumerate(detailLinks):
        dPage = cr.enterPage(dLink)
        data = cr.getDetailInf(dPage)
        print('i, len, partinf',i,len(detailLinks),partInfs[i])
        billId  = partInfs[i]['yyyymm']+str(partInfs[i]['id'])
        if not billId in ids: 
            #bills = cr.handleBillsInf(data,partInf[i])
            #print('bills:',bills)
            try:
                bills = cr.handleBillsInf(data,partInfs[i])
                db.insertBills(bills)
            except Exception as e:
                with open('report.txt','a') as f:
                    f.write('account: ' +acc+ '  \n' + partInfs[i]['id']+ partInfs[i]['yyyymm'] +'\nsomething wrong dealing with bill\n'+str(e)+'\n#####\n')
                    f.close()
                    print('account:',acc,' some thing wrong inserting bill')
        else:
            if dup > 2:
                break
            else:
                dup = dup + 1
                continue
 
    
def execute(acc,pw,cr,db):
    loginPage = cr.login(acc,pw)     
    links = cr.getHistoryLinks(loginPage)
    if not links:
        print('wrong account or wrong password!')
        return True
    
    #this parameter is for checking if the data are well executed or not
    wellExe = False
    for link in links:
        print('dealing with place:',link)
        page = cr.enterPage(link,False)
        #the taipower system is weired, it sometimes while logout during this procedure, so here I set a cheching machanism
        logout = cr.checkLogout(page)
        if logout:
            print('logout',logout)
            break
        partInfs = cr.getHistoryPageInf(page)
        #data = cr.getAllDetailInf(page)
        detailLinks = cr.getDetailLinks(page)
        #some places_id do not have any bill
        if not detailLinks:
            continue
        #check the whether places information exist or not
        dateReadN = db.getPlaceDateReadN(partInfs[0]['id'])
        print('dateReadN', dateReadN)
        if not dateReadN:
            doPlaceInf(detailLinks[0],partInfs[0],acc,pw,cr,db)
            doBillsInf(detailLinks,partInfs,acc,pw,cr,db)
        elif date.today() >= dateReadN[0]+timedelta(days=3):
            print(' reach the date of date_read_next+3, updating places...')
            updatePlaceInf(detailLinks[0],partInfs[0],acc,pw,cr,db)
            print(' reach the date of date_read_next+3, downloading new bills...')
            doBillsInf(detailLinks,partInfs,acc,pw,cr,db)
        else:
            print('not reach the date of date_read_next+3')
        cr.toFirstPage()
    wellExe = True
    
    return wellExe

if __name__ == '__main__':
    
    #accpw = ['42838254']
    dbase = Db()
    dbase.connect()
    accpw = dbase.getAllAcc()
    print("accpw   ",accpw)
    for t in accpw:
        crawl = CrawlBills()
        try:
            print('t:  \n',t)
            #sometimes system will logout,so I write some code to check whether it is wellexcuted or not
            wellExe = False
            while wellExe == False:
                #function execute run the whole crawling code
                wellExe = execute(t,t,crawl,dbase)
                if not wellExe:
                    with open('report.txt','a') as f:
                        f.write(t+'\n'+'not well executed\n')
                        f.close()

    
        except Exception as e:
            with open('report.txt','a') as f:
                f.write(t+'\n'+str(e)+'\n')
                f.close()
            print('error  ',t)
        crawl.endCrawlBills()

    dbase.closeConn()



