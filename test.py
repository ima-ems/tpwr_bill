from flask import Flask,render_template,request, redirect, url_for,g
from crawlbill import CrawlBills
from execute import execute
from db import Db
import multiprocessing

app = Flask(__name__)

def after_this_request(func):
    print('after_this_req')
    if not hasattr(g, 'call_after_request'):
        g.call_after_request = []
    g.call_after_request.append(func)
    return func

@app.after_request
def per_request_callbacks(response):
    print("per_req")
    for func in getattr(g, 'call_after_request', ()):
        response = func(response)
    return response

@app.route('/addAcc',methods=['POST'])
def start():
    acc = request.form.get('account')
    pw = request.form.get('password')
    exe(acc,pw)   
    print('conti') 
    #return render_template(url_for('.index'))
    return render_template('result.html')  
def exe(acc,pw):
    @after_this_request
    def addAcc(response):
        print('addAcc')
        #acc = request.form.get('account')
        #pw = request.form.get('password')
        print(acc,pw)
        #print('\n\n\n') 
        crawl = CrawlBills()
        dbase = Db()
        dbase.connect()
        try:
            #sometimes system will logout,so I write some code to check whether it is wellexcuted or not
            wellExe = False
            for i in range(10):
                #function execute run the whole crawling code
                wellExe = execute(acc,pw,crawl,dbase)
                if wellExe:
                    break
            if not wellExe:
                with open('report.txt','a') as f:
                    f.write(acc+'\n'+'not well executed\n')
                    f.close()
            print('result',wellExe)
        except Exception as e:
                with open('report.txt','a') as f:
                    f.write(acc+'\n'+str(e)+'\n')
                    f.close()
                print('error  ',acc)
    
    
        dbase.closeConn()
        crawl.endCrawlBills()
        #return render_template(url_for('index'))
        return response
@app.route('/')   
def index():    
    return render_template('addnewacc.html')




if __name__ == '__main__':
    app.run('0.0.0.0',debug = True)
