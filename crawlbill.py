import requests
import time
from datetime import date,timedelta
import re
from bs4 import BeautifulSoup
from db import Db
import googlemaps
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from pyvirtualdisplay import Display
from urllib.parse import urljoin

class CrawlBills:
    session = requests.Session()
    display = None
    #display = Display(visible=0, size=(1024, 768))
    #display.start()
    #profile = webdriver.FirefoxProfile()
    #profile.set_preference("intl.accept_languages", 'zh')
    #profile.update_preferences()
    #driver = webdriver.Firefox(profile)
    driver = None
    placesName = {'電號': 'id','用戶名稱':'ebpps_name','用電地址':'ebpps_add_p','通訊地址':'ebpps_add_b','統一編號':'company_id','用電種類':'class','饋線代號':'feeder','電表號碼':'aim','本次抄表日':'date_read','下次抄表日':'date_read_n','輪流停電組別':'group_pout','供電相線':'phasewire','供電電壓':'voltage','比流器':'ct','手機網路':'status_mobile','乙太網路':'status_ethernet','應繳總金額':'ta','服務單位':'tpc_servicer','用電計費期間':'pay_duration'}
    billsName = {'電號':'places_id','本次電費扣繳':'date_pstart','本次收費日':'date_pstart','用電度數':'kwh_total','應繳總金額':'ta','用電計費期間':'pay_duration','繳費期限':'date_pdue','功率因數：':'pf','功率因數[^調]':'pf','功率因數（％）':'pf','功率因數(%)':'pf','本期用電日數':'dop','流動電費':'fee_kwh','功率因數調整費：':'fee_pf','營業稅':'tax','經常.*契約':'ckw_pk','經常(尖峰)契約':'ckw_pk','經常（尖峰）契約':'ckw_pk','經常契約':'ckw_pk','契約容量':'ckw_pk','尖峰度數':'kwh_pk','周六半尖峰度數':'kwh_st','離峰度數':'kwh_hl','基本電費':'fee_kw','最高需量':'kw_pk','經常需量':'kw_pk','經常（尖峰）需量':'kw_pk','經常(尖峰)最高需量（瓩）':'kw_pk','分攤公共電費':'fee_pub','超約附加費':'fee_kwfine','半尖峰(>非夏月)需量':'kw_sp','半尖峰(非夏月)最高需量':'kw_sp','半尖峰（非夏月）需量':'kw_sp','離峰最高需量':'kw_hl','離峰需量':'kw_hl','離峰最高需量':'kw_hl','週六半尖峰需量':'kw_st','週六半尖峰最高需量':'kw_st','尖峰度數':'kwh_pk','尖峰用電度數':'kwh_pk','經常（尖峰）度數':'kwh_pk','經常用電度數':'kwh_pk','經常度數':'kwh_pk','半尖峰度數':'kwh_sp','半尖峰用電度數':'kwh_sp','離峰用電度':'kwh_hl','離峰度數':'kwh_hl','離峰用電度數':'kwh_hl','週六半尖峰用電度數':'kwh_st','週六半尖峰度數':'kwh_st'}


    def login(self,acc,pw):
        print('login......')
        self.display = Display(visible=0, size=(1024, 768))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        profile.set_preference("intl.accept_languages", 'zh')
        #profile.update_preferences()
        self.driver = webdriver.Firefox(profile)
 
        self.driver.get("https://ebpps.taipower.com.tw/EBPPS/action/conLogin.do?tmp=tmp")
        #driver.implicitly_wait(10)
        #print('driver:',driver)
        #ids = driver.find_elements_by_xpath('//*')
        #print('text',[t.text for t in ids])
        
        #element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.LINK_TEXT,'用戶登入')))
       # print(ids[0].find_elements_by_xpath('//*[@id]')[0].find_elements_by_xpath('//*[@id]')[0].text)
       # print(ids[1].find_elements_by_xpath('//*[@id]'))
       # print(ids[2][0].find_elements_by_xpath('//*[@id]'))
       # print(ids[0][1].find_element_by_tag_name('div'))
       # driver.find_element(By.LINK_TEXT,'用戶登入').click()
        
        element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, "moo_account")))
        username = self.driver.find_element_by_id("moo_account")
        username.send_keys(acc)
        self.driver.find_element(By.XPATH,'//*[@name="button"]').click()

        element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, "moofield")))
        password = self.driver.find_element_by_id("moofield")
        password.send_keys(pw)
        self.driver.find_element(By.XPATH,"//input[@onclick='return Check(this.form);']").click()
        #try:
        #    driver.find_element_by_xpath('//div[contains(@class,"ui-dialog") and @aria-describedby="dialogContent2"]//button[@title="Close"]').click()
        #    print('alert accepted')
        #except:
        #    print('no alert')
        
        #element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH,"//th[@class='sortable']")))
        
        try:
            self.driver.switch_to_alert().accept()
        except: 
            pass    
        data = self.driver.find_element(By.XPATH,"//div[@class='newtable']").get_attribute('innerHTML')
        cookies = self.driver.get_cookies()
        for cookie in cookies:
            self.session.cookies.set(cookie['name'], cookie['value'])
        
        print('data',data)
        self.driver.quit()
        self.display.stop()
        #wait for driver to stop
        time.sleep(2)
        return data 
        
#    def login(self,acc,pw):
#        r = self.session.get('https://ebpps.taipower.com.tw/EBPPS/action/conLogin.do?tmp=tmp')
#        payload = {'myAction': 'password', 'account': acc}
#        r = self.session.post("https://ebpps.taipower.com.tw/EBPPS/action/conLogin.do", data=payload)
#        print("cookie", r.cookies)
#        #headers = {'User-Agent': ':Mozilla/5.0 (Windows NT 10.0; WOW64) A    ppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36'}
#        payload2 = {'myAction':'check_PWD','words2':'HYCAPI_INSTEAD','old_hash':'J9zW0WIATNanaBv7pZ0pRcJMpT4=','words':pw}
#
#        #payload2 = {'myAction':'check_PWD','words2':'HYCAPI_INSTEAD','words':'42838254'}
#
#        r = self.session.post("https://ebpps.taipower.com.tw/EBPPS/action/conLogin.do", data=payload2)
#        return r.text

    def checkLogout(self,data):
        if re.search('電號',data):
            return False
        else:
            return True
        
    def getHistoryLinks(self,data):
        print('getting history links......')
        dom = BeautifulSoup(data, "lxml")
        l = []
        for link in dom.find_all('a'):
            if re.search('queryHistoryBill',link.get('href')):
                l.append(link.get('href'))
        print('historylinks  ',l)
        return l

    def getHistoryPageInf(self,page):
        print('getting history page information......')
        dom = BeautifulSoup(page,"lxml")
        l = []
        q = None
        #print('all tr',dom.find_all('tr')[0]) 
        for t in dom.find_all('tr'):
            data = {} 
            #print('tr',t.text)
            if re.search('用戶電號',t.text) and q is None:
                a = t.text.split('\n')
                q = a[a.index('用戶電號')+1].replace('-','').replace('\r','')
            if re.search('明細',t.text):
                a = t.text.replace(' ','').split('\n')
                #print("aaaa",a)
                data['id'] = q
                #check if they are the right information
                if  a[1] and a[3]:
                    #some old format do not have date_pdue at here
                    if a[5]:
                        data['date_pdue'] = a[5]
                    #handle format of 收費月份
                    x = a[1].replace('月','').split('年')
                    yyyymm  = str(int(x[0])+1911)+x[1]
                    data['yyyymm'] = yyyymm
                    data['ta'] = a[3]
                    l.append(data)
        #print("his page inf  :  ",l)
        return l 

    def getDetailLinks(self,page):
        print('getting detail links......')
        l = []
        dom = BeautifulSoup(page,"lxml")
        
        for link in dom.find_all('a'):
            if re.search('billDetail',link.get('href')):
                l.append(link.get('href'))
        return l
            
        
    def enterPage(self,link):
        time.sleep(1)
        print('enterpage' + link + '......')
        url = 'https://ebpps.taipower.com.tw'+link
        r = self.session.get(url)
        print('page data = \n',r.text)
        return r.text

    def getDetailInf(self,page):
        print('getting detail information......')
        l = []
        dom = BeautifulSoup(page,"lxml")
        keys = self.billsName.keys() 
        data = []
        for t in dom.find_all('td'):
            if any(word in t.text for word in keys):
                if len(t.text.split('\n')) > 10:
                    data = t.text.replace(' ','').replace('\r','').replace('\t','').replace('元','').replace(',','').replace('\u3000','').replace('*','').split('\n')
                    data = list(filter(None, data))
                    break
        #print('data :', data)
        return data

    def getAllInf(self,page):
        print('getting all information')
        dom =  BeautifulSoup(page,"lxml")
        #data = dom.find_all(name = "custInfoCSVFormat")
        data =  dom.find_all("input",attrs = {"name" : "custInfoCSVFormat"})
        inf = data[0].get('value')
        #print("inf", inf)

        #deal with the value
        l = inf.strip(' ').split("\n") 
        return l

    def handlePlacesInf(self,data,partInf,acc,pw):
        print('handling places information......')
        places = {}
        for i,t in enumerate(data):
            for word in self.placesName.keys():
                if re.search('^'+word,t):
                    try:
                        places[self.placesName[word]] = t.split('：')[1]
                    except:
                        pass
                    break
        print('places,partInf',places,partInf)
        
        #check the datail is in right order
        #there are some bill don't have ta in the lower table
        if 'ta' in places:
            if not (places['ta'] == partInf['ta'].replace(',','').replace('元','')):
                print('ta : ',  places['ta'],  partInf['ta'].replace(',','').replace('元',''))
                sys.exit("wrong order")
        else:
            pass
        
        places['created_at'] = time.strftime("%Y-%m-%d %H:%M:%S")
        places['id'] = partInf['id']
        places['ebpps_id'] = acc
        places['ebpps_pw'] = pw

        #handle date_read 抄表日 =收電迄日+1
        if 'pay_duration' in places:
            date_start,date_end = places['pay_duration'].split('至')
            x = date_end.split('年')
            a = x[1].replace('日','').split('月')
            date_read = date(int(x[0])+1911,int(a[0]),int(a[1]))
            date_read = date_read + timedelta(days=1)
            places['date_read'] ='/'.join([str(date_read.year),str(date_read.month),str(date_read.day)])

        
        # handle date_read_n
        if 'date_read_n' in places:
            #some bills have null value in date_read_n 
            if places['date_read_n']:
                x = None
                if re.search('年',places['date_read_n']):
                    x = places['date_read_n'].split('年')
                else:
                    x = places['date_read_n'].split('/',1)
                places['date_read_n'] = str(int(x[0])+1911) +'/' +x[1].replace('月','/').replace('日','') 
            else:
                del places['date_read_n']

        #handle geolocations
        gmaps = googlemaps.Client(key = 'AIzaSyAnj9uWAjYkTjZ6VydsfZGv-ujN1PaNqLw')
        test = gmaps.geocode(places['ebpps_add_p'].rsplit('號',1)[0]+'號')
        result = gmaps.geocode(places['ebpps_add_p'])
        
        #print('geoaddress  no 樓 ',result)
        #print('\n geoaddress  ', test) 
        if result:
            places['ebpps_add_g'] = result[0]['formatted_address']
            places['geocode_lat'] = result[0]['geometry']['location']['lat']
            places['geocode_lng'] = result[0]['geometry']['location']['lng']
            places['gplace_id'] = result[0]['place_id']
        if 'ta' in places:
            del places['ta']
        del places['pay_duration']
        print('placesFinal',places)
        return places


    def handleBillsInf(self,data,partInf):
        print('handling bill information......')
        bills = {}
        print('bill data', data)
        for i,t in enumerate(data):
            for word in self.billsName.keys():
                if re.search('^'+word,t):
                    try:
                        bills[self.billsName[word]] = t.split('：')[1]
                    except:
                        pass
                    break
        print('oriBill    ',bills)
        #there are some bill don't have ta in the lower table
        if 'ta' in bills:
            if not (bills['ta'] == partInf['ta'].replace(',','').replace('元','')):
                print('ta : ',  bills['ta'],  partInf['ta'].replace(',','').replace('元',''))
                sys.exit("wrong order")
        else:
            bills['ta'] = partInf['ta'].replace(',','').replace('元','')
        #handle format of 收費月份
        bills['yyyymm'] = partInf['yyyymm']
        
        #handle id
        bills['id'] = partInf['yyyymm']+str(partInf['id'])
        
        #handle places_id
        bills['places_id'] = partInf['id']
        
        #handle繳費期限 in detailPage
        if 'date_pdue' in bills:
            if re.search('年',bills['date_pdue']):
                x = bills['date_pdue'].split('年')
            else:
                x = bills['date_pdue'].split('/',1)
            bills['date_pdue'] = str(int(x[0])+1911) +'/' +x[1].replace('月','/').replace('日','') 


        #handle繳費期限 in partInf
        if 'date_pdue' in partInf:
            pdue = partInf['date_pdue'].split('/')
            pdue[0] =str(int(pdue[0])+1911)
            bills['date_pdue'] = '/'.join(pdue)
        
        #handle date_pstart
        if 'date_pstart' in bills:
            if re.search('年',bills['date_pstart']):
                x = bills['date_pstart'].split('年')
            else:
                x = bills['date_pdue'].split('/',1)
        
            bills['date_pstart'] = str(int(x[0])+1911) +'/' +x[1].replace('月','/').replace('日','') 
        

        #handle用電起訖
        date_start,date_end = bills['pay_duration'].split('至')
        x = date_start.split('年')
        date_start = str(int(x[0])+1911) +'/' + x[1].replace('月','/').replace('日','')
        x = date_end.split('年')
        date_end = str(int(x[0])+1911) +'/'+x[1].replace('月','/').replace('日','')
        bills['date_start'] = date_start
        bills['date_end'] = date_end

        #handle dop 
        if not 'dop' in bills:
            start = [int(n) for n in date_start.split('/')]
            start = date(start[0],start[1],start[2])
            end = [int(n) for n in date_end.split('/')]
            end = date(end[0],end[1],end[2])
            bills['dop'] = (end - start).days + 1
            print('no dop, end - start + 1= ', bills['dop']) 
        
        
        
        #handle date_read 抄表日 =收電迄日+1
        a = date_end.split('/')
        date_read = date(int(a[0]),int(a[1]),int(a[2]))
        date_read = date_read + timedelta(days=1)
        bills['date_read'] ='/'.join([str(date_read.year),str(date_read.month),str(date_read.day)])

        #handle taex and 日平均度數
        taex = sum(float(bills[t]) for t in ['fee_kw','fee_kwfine','fee_kwh','fee_pf'] if t in bills)/1.05
        bills['taex'] = taex
        #handle kwh_total
        bills['kwh_total'] = sum(int(bills[t]) for t in ['kwh_pk','kwh_sp','kwh_hl','kwh_st'] if t in bills)

        if 'kwh_total' in bills and bills['kwh_total'] != 0:
            bills['taex_pkwh'] = taex/float(bills['kwh_total'])
            if 'dop' in bills:
                bills['kwh_pday'] = float(bills['kwh_total'])/float(bills['dop']) 
        #elif 'kwh_pk' in bills and bills['kwh_pk'] != '0':
        #    bills['taex_pkwh'] = taex/float(bills['kwh_pk'])
        #    if 'dop' in bills:
        #        bills['kwh_pday'] = float(bills['kwh_pk'])/float(bills['dop']) 
        else:
            print('can not find kwh_total or kwh_pk in bills')

        #handle exclude
        bills['exclude'] = int(bills['ta']) - taex    
        
        #handle pf
        if not 'pf' in bills:
            bills['pf'] = '80'


        bills['created_at'] = time.strftime("%Y-%m-%d %H:%M:%S")
        
        del bills['pay_duration']
        #print('billFinal',bills)
        return bills
        

    #        if re.search('電號',t):
    #            places['id'] = data[i+5].replace('-','') 
    #        if re.search('用戶名稱',t):
    #            ebpps_name = t.split('：')[1]
    #            places['ebpps_name'] = ebpps_name
    #            continue
    #        if re.search('用電地址',t):
    #            ebpps_add_p = t.split('：')[1]
    #            places['ebpps_add_p'] = ebpps_add_p
    #            continue
    #        if re.search('統一編號',t):
    #            company_id = t.split('：')[1]
    #            places['company_id'] = company_id
    #            continue
    #        if re.search('用電種類',t):
    #            a = t.replace('\u3000',' ').split('： ')[1]
    #            places['class'] = a
    #            continue
    #        if re.search('饋線代號：',t):
    #            a = t.split('：' )[1]
    def endCrawlBills(self):
        self.driver.quit()                

            
        
if __name__ == '__main__':
    
            
    #session = requests.Session()
    db = Db()
    db.connect()
    cr = CrawlBills()
    acc = '42838254'
    pw = '42838254'
    loginPage = cr.login(acc,pw)
    links = cr.getHistoryLinks(loginPage)



#    for link in links:
#        page = enterPage(link)
#        inf = getAllInf(page)
#        print("inf:",inf)
#        placeId = db.getPlaceId(inf)
#        exist = db.checkExist(placeId)
#        print('exist: ',exist)
#        db.insertBill(inf)
        

#    page = enterPage(links[0])
#    inf = getAllInf(page)    
#    print("inf:",inf)
#    placeId = db.getPlaceId(inf)
#    exist = db.checkExist(placeId)
#    print('exist: ',exist)
#    db.insertBill(inf)

#    #for testing
#    page = enterPage(links[8])  
#    partInf = getHistoryPageInf(page)
#    detailLinks = getDetailLinks(page)
#    dPage = enterPage(detailLinks[0]) 
#    data = getDetailInf(dPage) 
#    places = handlePlacesInf(data,partInf[0],acc,pw) 
#    db.insertPlaces(places)
#    bills = handleBillsInf(data,partInf[0]) 
#    db.insertBills(bills) 


#    for link in links:
    for link in [l for l in links if re.search('00058401110',l)]:
        page = cr.enterPage(link)
        partInf = cr.getHistoryPageInf(page)
        detailLinks = cr.getDetailLinks(page)
        for i,dLink in enumerate(detailLinks):
            dPage = cr.enterPage(dLink)
            data = cr.getDetailInf(dPage)
            print('i, len, partinf',i,len(detailLinks),partInf[i]) 
            if not db.checkPlaceExist(partInf[i]['id']):
                places = cr.handlePlacesInf(data,partInf[i],acc,pw) 
                db.insertPlaces(places)
             
            billId  = partInf[i]['yyyymm']+str(partInf[i]['id'])
            if not db.checkBillExist(billId):
                bills = cr.handleBillsInf(data,partInf[i])
                #print('bills:',bills)
                db.insertBills(bills)
            else:
                continue

