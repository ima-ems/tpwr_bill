import sys
from mysql.connector import MySQLConnection, Error

class Db:
    db_config = {'password': '42838254', 'host': 'localhost', 'user': 'root', 'database': 'tpcbills'}
    frameName = {'電號':'places_id','收費月份':'yyyymm','用電度數(度)':'kwh_total','應繳總金額':'ta','繳費期限':'date_pdue','功率因數':'pf','功率因數(%)':'pf','經常度數':'kwh_pk','尖峰度數':'kwh_pk','本期用電日數':'dop','流動電費':'fee_kwh','功率因數調整費':'fee_pf','營業稅':'tax','經常(尖峰)契約':'ckw_pk','經常(尖峰)需量':'kw_pk','離峰需量':'kw_hl','尖峰度數':'kwh_pk','周六半尖峰度數':'kwh_st','周六半尖峰需量':'kw_st','離峰度數':'kwh_hl','基本電費':'fee_kw','經常契約':'ckw_pk','經常需量':'kw_pk','分攤公共電費':'fee_pub','超約附加費':'fee_kwfine','半尖峰(非夏月)需量':'kw_sp','半尖峰度數':'kwh_sp'}
    conn = None
    cursor = None
    def connect(self):
        try:
            print('Connecting to MySQL database...')
            self.conn = MySQLConnection(**self.db_config)
            self.cursor = self.conn.cursor()

            if self.conn.is_connected():
                print('connection established.')
            else:
                print('connection failed.')
        except Error as e:
            print('Error:', e)
    def initTables(self):
        query =[ "CREATE TABLE place_tpcbills(id char(17),places_id char(11) , yyyymm char(6),date_read datetime,date_start datetime,date_end datetime,date_pstart datetime,date_pdue datetime,dop decimal(3,0),ta decimal(18,0),fee_kw decimal(20,2),fee_kwfine decimal(20,2),fee_kwh decimal(20,2),fee_pf decimal(20,2),fee_pub decimal(20,2),fee_oth decimal(20,2),tax decimal(20,2),exclude decimal(20,2),taex decimal(18,0),kwh_total decimal(18,0),kwh_pk decimal(18,0),kwh_sp decimal(18,0),kwh_hl decimal(18,0),kwh_st decimal(18,0),ckw_pk decimal(18,0),ckw_sp decimal(18,0),ckw_hl decimal(18,0),ckw_st decimal(18,0),kw_pk decimal(18,0),kw_sp decimal(18,0),kw_hl decimal(18,0),kw_st decimal(18,0),pf decimal(3,0),taex_pkwh double,kwh_pday double,created_at datetime,status char(8),CONSTRAINT pk_id PRIMARY KEY (id))ENGINE=InnoDB DEFAULT CHARSET=utf8;","CREATE TABLE places(id char(11),ebpps_id varchar(11),ebpps_pw varchar(11),ebpps_name varchar(31),ebpps_add_p varchar(63),ebpps_add_b varchar(63),ebpps_add_g varchar(63),geocode_lat decimal(10,7),geocode_lng decimal(10,7),gplace_id varchar(255),company_id char(8),class varchar(31),feeder char(4),aim varchar(11),date_read date,date_read_n date,group_read char(1),group_pout char(1),tpc_servicer varchar(11),phasewire char(4),voltage decimal(6,0),ct varchar(7),status_mobile char(8),status_ethernet char(8),status_tpcbill char(8),created_at date,update_at date,status char(8),CONSTRAINT pk_id PRIMARY KEY (id))ENGINE=InnoDB DEFAULT CHARSET=utf8;"]
        self.connect()
        for q in query:
            self.cursor.execute(q)
        self.conn.commit()

        self.cursor.close()
        self.conn.close()
    def insertBill(self,inf):
        self.connect()
        l = []
        #there is a null list at the end of the inf list,I'll delete it.
        inf.pop(-1)
        name = inf[0].split(',')
        #to get the index of the item for generating yyyymm
        idIndex = (name.index('電號'),name.index('收費月份'))
        print("name:",name) 
        #get the index of every needed item, and also get its frame name in SQL by checking the dictionary
        for i,title in enumerate(name):
            if title in self.frameName:
                l.append((i,self.frameName[title]))
        print(l)
        
        for i,x in enumerate(inf):
            if i == 0:
                continue     
            values = inf[i].replace('*','').replace('元','').split(',')
            billId = self.addId(values,idIndex)
            #handle format of 電號 
            values[name.index('電號')] = values[name.index('電號')].replace('-','')
            #handle format of 收費月份
            x = values[name.index('收費月份')].strip('月').split('年')
            values[name.index('收費月份')] = str(int(x[0])+1911)+x[1]
            if values[name.index('繳費期限')]:
                pdue = values[name.index('繳費期限')].split('/')
                pdue[0] =str(int( pdue[0])+1911)
                values[name.index('繳費期限')] = '/'.join(pdue)
            values = [values[t[0]] for t in l ]
            print("values : ",values)
            #delete null items
            if not all(values):
                print("not all ")
                #nullIndex = values.index('')
                nullIndex = [i for i, x in enumerate(values) if x == ""]
                print("nullIndex",nullIndex)
                nullIndex.sort(reverse = True)
                for i in nullIndex:
                        del values[i]
                        del l[i]

            print("values : ",values)
            values.append(billId) 
                
            query = "INSERT INTO place_tpcbills(" + ','.join([item[1] for item in l])+ ",id ) VALUES("+','.join(['%s']*(len(l)+1))+ ")"
            print('query:',query)
            self.cursor.execute(query,values)
        self.conn.commit()
        self.cursor.close()
        self.conn.close()

    def addId(self,inf,index):
        #handle 收費年月
        print('index:',index)
        x = inf[index[1]].strip('月').split('年')
        print(" inf[index[0 and 1]]  ", inf[index[0]].strip('-'), inf[index[1]])
        x = str(int(x[0])+1911)+x[1]
        billId = inf[index[0]].replace('-','')+x
        return billId    
    #def handleValues(values): 
    def getPlaceId(self,inf):
        name = inf[0].split(',')
        placeId =inf[1].split(',')[name.index('電號')]
        placeId = placeId.replace('-','')
        return placeId

    def checkExist(self,placeId):
        self.connect()
        self.cursor.execute('SELECT id FROM places WHERE id = '+placeId)
        row = self.cursor.fetchone()
        if row is not None:
            return True
        else:
            return False
        self.cursor.close()
        self.conn.close()


if __name__ == "__main__":
    db = Db()
    db.initTables()
