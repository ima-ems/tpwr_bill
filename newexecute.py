from db import Db
from newcrawlbill import CrawlBills


def execute(acc,pw,cr,db):
    loginPage = cr.login(acc,pw)     
    links = cr.getHistoryLinks(loginPage)
    if not links:
        print('wrong account or wrong password!')
        return True
    #this parameter is for checking if the data are well executed or not
    wellExe = False
    for link in links:
        print('dealing with place:\n',link)
        page = cr.enterPage(link,False)
        #the taipower system is weired, it sometimes while logout during this procedure, so here I set a cheching machanism
        logout = cr.checkLogout(page)
        if logout:
            print('logout',logout)
            break
        partInf = cr.getHistoryPageInf(page)
        detailLinks = cr.getDetailLinks(page)
        #some places_id do not have any bill
        if not detailLinks:
            cr.toFirstPage()
            continue
        #check the whether places information exist or not
        if not db.checkPlaceExist(partInf[0]['id']):
            try:
                dPage = cr.enterDetailPage(detailLinks[0])
                data = cr.getDetailInf(dPage)
                places = cr.handlePlacesInf(data,partInf[0],acc,pw)
                db.insertPlaces(places)
            except Exception as e:
                with open('report.txt','a') as f:
                    f.write(acc + partInf[0]['id']+'\nsomething wrong dealing with places\n'+str(e)+'\n')
                    f.close()
                    print('some thing wrong dealing with places')

        #load the places_id in databases if it had been stored than ignore it, load all ids at once to make it more effcient
        ids = db.getBillIds(partInf[0]['id'])
        if len(ids) == len(partInf):
            cr.toFirstPage()
            continue
        for i,dLink in enumerate(detailLinks):
            dPage = cr.enterDetailPage(dLink)
            data = cr.getDetailInf(dPage)
            print('i, len, partinf',i,len(detailLinks),partInf[i])
            billId  = partInf[i]['yyyymm']+str(partInf[i]['id'])
            if not billId in ids: 
                #bills = cr.handleBillsInf(data,partInf[i])
                #print('bills:',bills)
                try:
                    bills = cr.handleBillsInf(data,partInf[i])
                    db.insertBills(bills)
                except Exception as e:
                    with open('report.txt','a') as f:
                        f.write(partInf[i]['id']+ partInf[i]['yyyymm'] +'\nsomething wrong dealing with bill\n'+str(e)+'\n')
                        f.close()
                        print('some thing wrong inserting bill')
            else:
                continue
                #break
        cr.toFirstPage()
    cr.endCrawlBills()
    wellExe = True
    return wellExe

if __name__ == '__main__':
    accpw3 = ['00057609','00070883','00082403','00146791','00146877','00149903','00199184','00241084','00262453','00280902','00371109','00385309','00388501','00430000','00432106','00508409','00650329','00658841']
    accpw2 = ['00662465','00669256','00676756','00688129','00700860','00701122','00703705','00708835','00713181','00714908','00730294','00730941','00802260','00805344','00807526','00833117','00847560','00853422','00863759','00864574','00894606','00908518','00927352','00933164','00965502','00966713','00974973','00985768','01012145','01016515','01017232','01032780','01061855','01175920','01180613','01181346','01183554','01188755','01289016','01348275','01356788','01456077','01506132','01516589','01630738','01634087','01801726','01869627','01884606','01887131','01887370','01923544','01978049','01982682','02005810','02033409','02084455','02119659','02133548','02153831','02219664','02224239','02252993','02254182','02255204','02256885','02291736','02300242','02331218','02391144','02413153','02426431','02442946','02445866','02453154','02612744','02625902','02627704','02631159','02631685','02631849','02634347','02686098','02695619','02750963','02770216','02797952','02809280','02810580','02849446','02857493','02890943','02900127','02908991','02927792','02993997','03025201','03028426','03042004','03064800','03088401','03088704','03088802','03089008','03118405','03178418','03213392','03222202','03228304','03250706','03276706','03315911','03358805','03375001','03392022','03407502','03430449','03434016','03434402','03458403','03474311','03480910','03504928','03538906','03543210','03550210','03553526','03558206','03564501','03564609','03623706','03701104','03702716','03705903','03708205','03710808','03714403','03716423','03717485','03717491','03724401','03737200','03741302','03750343','03751103','03751671','03753708','03771608','03774605','03788404','03795101','03812404','03812501','04123102','04123481']

    accpw = ['22662257']
    dbase = Db()
    dbase.connect()
    for t in accpw:
        crawl = CrawlBills()
        try:
            print('t:  \n',t)
            #sometimes system will logout,so I write some code to check whether it is wellexcuted or not
            wellExe = False
            #while wellExe == False:
            for i in range(15):
                #function execute run the whole crawling code
                wellExe = execute(t,t,crawl,dbase)
                if not wellExe:
                    with open('report.txt','a') as f:
                        f.write(t+'\n'+'not well executed\n')
                        f.close()
                else:
                    break
    
        except Exception as e:
            with open('report.txt','a') as f:
                f.write(t+'\n'+str(e)+'\n')
                f.close()
            print('error  ',t)

    dbase.closeConn()
    #crawl.endCrawlBills()



