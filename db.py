import sys
from mysql.connector import MySQLConnection, Error

class Db:
    db_config = {'password': '42838254', 'host': '125.227.141.38', 'user': 'ima', 'database': 'ima-lab'}
    #db_config = {'password': '42838254', 'host': '125.227.141.39', 'user': 'root', 'database': 'tpcbills'}
    frameName = {'電號':'places_id','收費月份':'yyyymm','用電度數(度)':'kwh_total','應繳總金額':'ta','繳費期限':'date_pdue','功率因數':'pf','功率因數(%)':'pf','經常度數':'kwh_pk','尖峰度數':'kwh_pk','本期用電日數':'dop','流動電費':'fee_kwh','功率因數調整費':'fee_pf','營業稅':'tax','經常(尖峰)契約':'ckw_pk','經常(尖峰)需量':'kw_pk','離峰需量':'kw_hl','尖峰度數':'kwh_pk','周六半尖峰度數':'kwh_st','周六半尖峰需量':'kw_st','離峰度數':'kwh_hl','基本電費':'fee_kw','經常契約':'ckw_pk','經常需量':'kw_pk','分攤公共電費':'fee_pub','超約附加費':'fee_kwfine','半尖峰(非夏月)需量':'kw_sp','半尖峰度數':'kwh_sp'}
    conn = None
    cursor = None
    def connect(self):
        try:
            print('Connecting to MySQL database...')
            self.conn = MySQLConnection(**self.db_config)
            self.cursor = self.conn.cursor()
            print(self.cursor)
            if self.conn.is_connected():
                print('connection established.')
            else:
                print('connection failed.')
        except Error as e:
            print('Error:', e)
    def initTables(self):
        query =[ "CREATE TABLE place_tpcbills(id char(17),places_id char(11) , yyyymm char(6),date_read date,date_start date,date_end date,date_pstart date,date_pdue date,dop decimal(3,0),ta decimal(18,0),fee_kw decimal(20,2),fee_kwfine decimal(20,2),fee_kwh decimal(20,2),fee_pf decimal(20,2),fee_pub decimal(20,2),fee_oth decimal(20,2),tax decimal(20,2),exclude decimal(20,2),taex decimal(18,0),kwh_total decimal(18,0),kwh_pk decimal(18,0),kwh_sp decimal(18,0),kwh_hl decimal(18,0),kwh_st decimal(18,0),ckw_pk decimal(18,0),ckw_sp decimal(18,0),ckw_hl decimal(18,0),ckw_st decimal(18,0),kw_pk decimal(18,0),kw_sp decimal(18,0),kw_hl decimal(18,0),kw_st decimal(18,0),pf decimal(3,0),taex_pkwh double,kwh_pday double,created_at datetime,status char(8),CONSTRAINT pk_id PRIMARY KEY (id))ENGINE=InnoDB DEFAULT CHARSET=utf8;","CREATE TABLE places(id char(11),ebpps_id varchar(11),ebpps_pw varchar(11),ebpps_name varchar(31),ebpps_add_p varchar(63),ebpps_add_b varchar(63),ebpps_add_g varchar(120),geocode_lat decimal(10,7),geocode_lng decimal(10,7),gplace_id varchar(255),company_id char(8),class varchar(31),feeder char(4),aim varchar(11),date_read date,date_read_n date,group_read char(1),group_pout char(1),tpc_servicer varchar(11),phasewire char(4),voltage decimal(6,0),ct varchar(7),status_mobile char(8),status_ethernet char(8),status_tpcbill char(8),created_at datetime,update_at datetime,status char(8),CONSTRAINT pk_id PRIMARY KEY (id))ENGINE=InnoDB DEFAULT CHARSET=utf8;"]
        self.connect()
        for q in query:
            self.cursor.execute(q)
        self.conn.commit()

        self.cursor.close()
        self.conn.close()

    def insertPlaces(self,data):
        #self.connect()
        values = list(data.items())

        query = "INSERT INTO places(" + ','.join([item[0] for item in values])+ " ) VALUES("+','.join(['%s']*(len(values)))+ ")"
        print('query:',query)
        self.cursor.execute(query,[v[1] for v in values])
        self.conn.commit()
        #self.cursor.close()
        #self.conn.close()

    def updatePlace(self,data):
        placeid = data['id']
        values = list(data.items())
        #query = "UPDATE places SET "
        query = "UPDATE places SET "+",".join(["`"+ str(item[0])+"`"+" = "+"'"+str(item[1])+"'"  for item in values]) + "WHERE `id` = '"+ placeid + "'"
        print('update query:',query)
        self.cursor.execute(query)
        self.conn.commit()
 
    def insertBills(self,data):
        #self.connect()
        values = list(data.items())
                 
        query = "INSERT INTO place_tpcbills(" + ','.join([item[0] for item in values])+ " ) VALUES("+','.join(['%s']*(len(values)))+ ")"
        print('query:',query)
        self.cursor.execute(query,[v[1] for v in values])
        self.conn.commit()
        #self.cursor.close()
        #self.conn.close()
        print('end query')

    def checkPlaceExist(self,placeId):
        #self.connect()
        self.cursor.execute('SELECT id FROM places WHERE id = '+placeId)
        row = self.cursor.fetchone()
        print('return places row',row)
        if row is not None:
            return True
        else:
            return False
        #self.cursor.close()
        #self.conn.close()
   
    def getPlaceDateReadN(self,placeId):
        print('get place date read n ...')
        self.cursor.execute('SELECT date_read_n FROM places WHERE id = ' +placeId)
        row = self.cursor.fetchone()
        print('return', row)
        return row
        
    def checkBillExist(self,billId):
        print('getting bill id...')
        #self.connect()
        self.cursor.execute('SELECT id FROM place_tpcbills WHERE id = '+billId)
        row = self.cursor.fetchone()
        if row is not None:
            return True
        else:
            return False
        #self.cursor.close()
        #self.conn.close()

    def getBillIds(self,placesId):
        print('getting bills id.....')
        self.cursor.execute('SELECT id FROM place_tpcbills WHERE places_id  = '+placesId)
        rows = self.cursor.fetchall()
        print('\nreturn bill rows ',[r[0] for r in rows] )
        return [r[0] for r in rows]
        #self.cursor.close()

    def getAllAcc(self,):
        self.cursor.execute("SELECT id FROM companies WHERE `status` = '30000001'")
        rows = self.cursor.fetchall()
        return [r[0] for r in rows]

    def closeConn(self):
        self.cursor.close()
        self.conn.close()


if __name__ == "__main__":
    db = Db()
    db.initTables()
